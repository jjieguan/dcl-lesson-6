const pivot = new Entity()
engine.addEntity(pivot)
pivot.addComponent(new Transform())
pivot.getComponent(Transform).position.set(16,0,16)

let spheres = []
for (let j = 1; j < 4; j = j + 1) {
    for (let i = 1; i < 9; i = i + 0.1) {
        spheres[i*j] = spawnSphere()
        //Material
        const myMaterial = new Material()
        myMaterial.albedoColor = new Color3(i*j/7 , 0, 0.5)
        spheres[i*j].addComponent(myMaterial)
    }
}

//set up sphere
export class SimpleMove implements ISystem {
    update() {

        for (let j = 1; j < 4; j = j + 1) {
            for (let i = 1; i < 9; i = i + 0.1) {
                const angle = i * Math.PI * 2 / 8
                const radius = i*j/4
                let x = Math.cos(angle) * radius
                let z = Math.sin(angle) * radius
                spheres[i*j].getComponent(Transform).position.set(x, j*i/4, z)
                spheres[i*j].getComponent(Transform).scale.set(0.1, 0.1, 0.1)

            }
        }

    }
}
engine.addSystem(new SimpleMove())

//moving sphere
let py = 0
export class PivotRotate implements ISystem {
    
    update() {
        
        py=py+1
        pivot.getComponent(Transform).rotation.setEuler(0,py,0)
        
    }
}
engine.addSystem(new PivotRotate())

function spawnSphere() {
    const sphere = new Entity()
    sphere.addComponent(new SphereShape())

    sphere.addComponent(new Transform())
    engine.addEntity(sphere)
    //Material
    

    sphere.setParent(pivot)
    return sphere
}

